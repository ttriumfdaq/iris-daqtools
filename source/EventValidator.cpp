#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "EventValidator.h"
#include "AdcEvent.h"
#include "TdcEvent.h"

#include "midas.h"

using namespace std;

EventValidator::EventValidator(string type, int n)
{
  moduleEvents.resize(n);
  for(int i=0; i<n; i++){
    if(type == "ADC") moduleEvents.at(i) = shared_ptr<ModuleEvent>(new AdcEvent());
    else if(type == "TDC") moduleEvents.at(i) = shared_ptr<ModuleEvent>(new TdcEvent());
    else{
      printf("EventValidator::EventValidator(): Error! Unknown type %s.\n",type.c_str());
      exit(EXIT_FAILURE);
    }
  }
  tolerance = 0;
  roll_over = 63;
}

bool EventValidator::Complete()
{
  int i;
  for(i=0; i<moduleEvents.size(); i++){
    if(!moduleEvents.at(i)->Complete()) return false;
  }
  return true;
}

bool EventValidator::CheckConsistency()
{
  const uint64_t id0 = moduleEvents.at(0)->EventId();
  //printf("EventValidator::CheckConsistency(): id0 = 0x%016llx\n",id0);
  int i;
//  for(i = 1; i<moduleEvents.size(); i++){
//    shared_ptr<ModuleEvent> evt_i = moduleEvents.at(i);
    
  for(i = 1; i<moduleEvents.size(); i++){
    shared_ptr<ModuleEvent> evt_i = moduleEvents.at(i);
    //We allow for a difference of one unit between the IDs. Nasty comp. of doubles.
    uint64_t idi = evt_i->EventId();
    //printf("ID for module %d: 0x%016x.\n",i,idi);
    if(idi != id0){
      //Ok, not exactly equal, we allow for almost equal also.
      uint64_t absdiff = idi > id0 ? idi - id0 : id0 - idi;
      //printf("EventValidator::CheckConsistency(): abs(idi-id0) = 0x%016llx\n",absdiff);
      //printf("EventValidator::CheckConsistency(): id0 = 0x%016llx, idi = 0x%016llx\n",id0,idi);
      uint64_t maxdiff = (uint64_t)1<<roll_over;
      //if (absdiff != 1) {
      //printf("EventValidator: timestamp mismatch module %d, id0 0x%016lx, idi 0x%016lx, absdiff 0x%016lx, roll_over %d, maxdiff 0x%016lx, tolerance %d\n", i, id0, idi, absdiff, roll_over, maxdiff, tolerance);
      //}
      if(absdiff > tolerance && absdiff < maxdiff-tolerance) {
	cm_msg(MERROR, "EventValidator", "timestamp mismatch module %d, id0 0x%016lx, idi 0x%016lx, absdiff 0x%016lx, roll_over %d, maxdiff 0x%016lx, tolerance %d", i, id0, idi, absdiff, roll_over, maxdiff, tolerance);
	return false;
      }
    }
  }
  //printf("Event consistent\n");
  return true; 
}

//If any events show a lower timestamp/ID than any other, it belongs to an
//incomplete event and must be thrown away.
void EventValidator::Purge()
{
  uint64_t idmax = 0;
  int module;
  int i;
  for(i=0; i<moduleEvents.size(); i++){
    shared_ptr<ModuleEvent> evt_i = moduleEvents.at(i);
    uint64_t idi = evt_i->EventId();
    if(idi > idmax){
      idmax = idi;
      module = i;
    }
  }
  int nErr = 0;
  for(i=0; i<moduleEvents.size(); i++){
    shared_ptr<ModuleEvent> evt_i = moduleEvents.at(i);
    uint64_t idi = evt_i->EventId();
    uint64_t diff = idmax - idi;
    if(diff > tolerance){
      evt_i->Clear();
      nErr++;
    }
  }
  //return module;
  /*
  for(int i=0; i<moduleEvents.size(); i++){
    shared_ptr<ModuleEvent> evt_i = moduleEvents.at(i);
    printf("Module %d:\n",i);
    while(!evt_i->Empty()){
      printf("  0x%08x\n",evt_i->Front());
      evt_i->UnsafePop();
    }
  }
  */
  //printf("EventValidator::Purge(): %d module-events cleared.\n",nErr);   
}

void EventValidator::SetTolerance(uint8_t delta)
{
  tolerance = delta;
}
    
void EventValidator::SetRollOver(uint8_t nBits)
{
  roll_over = nBits;
}

uint8_t EventValidator::GetTolerance()
{
  return tolerance;
}

uint8_t EventValidator::GetRollOver()
{
  return roll_over;
}

bool EventValidator::Empty()
{
  for(auto evt : moduleEvents){
    if(!evt->Empty()) return false;
  }
  return true;
}

void EventValidator::Clear()
{
  for(auto evt : moduleEvents){
    evt->Clear();
  }
}
