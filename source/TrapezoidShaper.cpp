#include "TrapezoidShaper.h"

TrapezoidShaper::TrapezoidShaper(int decayTime, int riseTime, int topWidth)
  : fDecayTime(decayTime), fRiseTime(riseTime), fTopWidth(topWidth) {}
    
double TrapezoidShaper::d(double *signal, int k, int l, int j)
{
  return signal[j] - signal[j-k] - signal[j-l] + signal[j-k-l];
}
    
double * TrapezoidShaper::Shape(double *signal, int length)
{
  fShaped.clear();

  //We apply the trapezoidal filter. Naming conventions as in Jordanov+Knoll.
  int M = fDecayTime;
  int m = fTopWidth;
  int k = fRiseTime;
  int l = m + k;

  double pp = 0.;
  double s = 0.;
  for(int i=0; i<length; i++){
    if(i<(2*k+m)){ fShaped.push_back(0); continue;}
    pp += d(signal,k,l,i);
    s += (pp + M * d(signal,k,l,i))/((double)k*M);
    fShaped.push_back(s);
  }
  
  return fShaped.data();
}


void TrapezoidShaper::SetDecayTime(int time)
{
  fDecayTime = time;
}

void TrapezoidShaper::SetRiseTime(int time)
{
  fRiseTime = time;
}

void TrapezoidShaper::SetTopWidth(int width)
{
  fTopWidth = width;
}
  
int TrapezoidShaper::GetDecayTime()
{
  return fDecayTime;
}

int TrapezoidShaper::GetRiseTime()
{
  return fRiseTime;
}

int TrapezoidShaper::GetTopWidh()
{
  return fTopWidth;
}
