#include "ModuleEvent.h"
#include <cstdio>

using namespace std;

uint32_t ModuleEvent::Front()
{
  lock_guard<mutex> lock(buffer_mutex);
  return data.front();
};

bool ModuleEvent::Empty()
{
  lock_guard<mutex> lock(buffer_mutex);
  return data.empty();
};

void ModuleEvent::Pop()
{
  lock_guard<mutex> lock(buffer_mutex);
  if(!complete){
    printf("ModuleEvent::Pop(): Error! Event not complete.\n");
    return;
  }
  //lock_guard<mutex> lock(buffer_mutex);
  data.pop();
  if(data.empty()){
    eventId = 0;
    complete = false;
  }
}

void ModuleEvent::UnsafePop()
{
  lock_guard<mutex> lock(buffer_mutex);
  data.pop();
  if(data.empty()){
    eventId = 0;
    complete = false;
  }
}

void ModuleEvent::Clear()
{
  lock_guard<mutex> lock(buffer_mutex);
  std::queue<uint32_t> empty;
  std::swap(data, empty);
  eventId = 0;
  complete = false;
}

bool ModuleEvent::Complete()
{
  lock_guard<mutex> lock(buffer_mutex);
  return complete;
}
    
uint64_t ModuleEvent::EventId()
{
  lock_guard<mutex> lock(buffer_mutex);
  return eventId;
}

unsigned int ModuleEvent::Size()
{
  lock_guard<mutex> lock(buffer_mutex);
  return data.size();
}
