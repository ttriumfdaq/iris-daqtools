#include "ConstantFractionAnalyser.h"

ConstantFractionAnalyser::ConstantFractionAnalyser
  (double attenuation, int delay, bool negative)
  : fAttenuation(attenuation), fDelay(delay), Analyser(negative) {}

double ConstantFractionAnalyser::Analyse(double *signal, int length)
{
  double fResult = 0;
  double cfd_signal_prev = fAttenuation * signal[fDelay] - signal[0];
  for(int i=fDelay+1; i<length; i++){
    double cfd_signal = fAttenuation * signal[i] - signal[i-fDelay];
    if((!fPolarity && cfd_signal < 0) || (fPolarity && cfd_signal > 0)){
      //We had a zero-crossing. Make linear interpolation.
      fResult = i - cfd_signal / (cfd_signal - cfd_signal_prev);
      break;
    }
    cfd_signal_prev = cfd_signal;
  }
  return fResult;
}

void ConstantFractionAnalyser::SetAttenuation(double attenuation)
{
  fAttenuation = attenuation;
}
    
double ConstantFractionAnalyser::GetAttenuation()
{
  return fAttenuation;
}
    
void ConstantFractionAnalyser::SetDelay(int delay)
{
  fDelay = delay;
}
    
int ConstantFractionAnalyser::GetDelay()
{
  return fDelay;
}
