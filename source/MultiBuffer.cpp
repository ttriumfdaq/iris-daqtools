#include "MultiBuffer.h"

void MultiBuffer::push(int i, const uint32_t &word)
{
  buffers.at(i).push(word);
  totalSize++;
}

bool MultiBuffer::empty()
{
  /*
  unsigned int i;
  for(i=0; i<buffers.size(); i++){
    if(!buffers.at(i).empty()) return false;
  }
  return true;
  */
  if(totalSize == 0) return true;
  else return false;
}

unsigned int MultiBuffer::size()
{
  /*
  unsigned int i;
  size_t size = 0;
  for(i=0; i<buffers.size(); i++){
    size += buffers.at(i).size();
  }
  return size;
  */
  return totalSize;
}

void MultiBuffer::clear()
{
  for(int i=0; i<buffers.size(); i++){
    buffers.at(i).clear();
  }
  totalSize = 0;
}
