#include "Analyser.h"

Analyser::Analyser(bool negative) : fPolarity(negative) {}

void Analyser::SetPolarity(bool negative){fPolarity = negative;}
    
bool Analyser::GetPolarity(){ return fPolarity;}
    
double Analyser::GetResult(){ return fResult;}
