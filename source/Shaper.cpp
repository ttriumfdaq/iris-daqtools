#include "Shaper.h"

using namespace std;

double * Shaper::Shape(uint32_t *signal, int length)
{
  vector<double> d_signal (length);
  for(int i=0; i<length; i++){
    d_signal.at(i) = (double) (signal[i]);
  }
  return Shape(d_signal.data(),length);
}

double * Shaper::Shape(int *signal, int length)
{
  vector<double> d_signal (length);
  for(int i=0; i<length; i++){
    d_signal.at(i) = (double) (signal[i]);
  }
  return Shape(d_signal.data(),length);
}

vector<double> & Shaper::GetShaped()
{
  return fShaped;
}
