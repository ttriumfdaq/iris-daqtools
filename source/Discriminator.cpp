#include "Discriminator.h"

using namespace std;

Discriminator::Discriminator(double deadTime) : fDeadTime(deadTime) {}

vector<double> & Discriminator::GetTriggers() {return fTriggers;}
    
void Discriminator::SetDeadTime(double deadTime) { fDeadTime = deadTime;}
    
double Discriminator::GetDeadTime() { return fDeadTime;}
