#include "AmplitudeAnalyser.h"
#include <limits>

using namespace std;

AmplitudeAnalyser::AmplitudeAnalyser(bool negative) : Analyser(negative) {}
    
double AmplitudeAnalyser::Analyse(double *signal, int length)
{
  fResult = 0;
  if(!fPolarity){
    //Positive polarity.
    fResult = numeric_limits<double>::min();
    for(int i=0; i<length; i++){
      if(signal[i] > fResult) fResult = signal[i];
    }
  }
  else{
    //Negative polarity.
    fResult = numeric_limits<double>::max();
    for(int i=0; i<length; i++){
      if(signal[i] < fResult) fResult = signal[i];
    }
  }
  
  return fResult;
}
