#include "LeadingEdgeDiscriminator.h"
#include <cmath>

using namespace std;

LeadingEdgeDiscriminator::LeadingEdgeDiscriminator
  (double threshold, bool negative)
  : fThreshold(threshold), fPolarity(negative) {}
  
    
int LeadingEdgeDiscriminator::Analyse(double *signal, int length)
{
  fTriggers.clear();
  
  if(!fPolarity){
    //Positive polarity.
    bool above_prev = signal[0] > fThreshold ? true : false;
    for(int i=1; i<length; i++){
      bool above = signal[i] > fThreshold ? true : false;
      if(above && !above_prev){
        //We crossed the threshold between i-1 and i. Linear interpolation.
        double time = (fThreshold - signal[i])/(signal[i] - signal[i-1]) + i;
        fTriggers.push_back(time);
        i += lrint(fDeadTime);
        above_prev = above;
      }
      above_prev = above;
    }    
  }
  else{
    //Negative polarity.
    bool below_prev = signal[0] < fThreshold ? true : false;
    for(int i=1; i<length; i++){
      bool below = signal[i] < fThreshold ? true : false;
      if(below && !below_prev){
        //We crossed the threshold between i-1 and i. Linear interpolation.
        double time = (fThreshold - signal[i])/(signal[i] - signal[i-1]) + i;
        fTriggers.push_back(time);
        i += lrint(fDeadTime);
        below_prev = below;
      }
      below_prev = below;
    }
  }
  
  return fTriggers.size();
}
    
void LeadingEdgeDiscriminator::SetThreshold(double threshold)
{
  fThreshold = threshold;
}
    
void LeadingEdgeDiscriminator::SetPolarity(bool negative)
{
  fPolarity = negative;
}
    
double LeadingEdgeDiscriminator::GetThreshold()
{
  return fThreshold;
}
    
bool LeadingEdgeDiscriminator::GetPolarity()
{
  return fPolarity;
}
