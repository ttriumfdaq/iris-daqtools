# DaqTools

This project provides tools that are used by the Iris DAQ frontend programs. The
classes are mainly related to event reconstruction and data validation for data
from Mesytec MADC-32 ADC modules and CAEN V1190A/B TDC modules. It also
provides tools for trapezoidal pulse shaping and analysis, necessary for dealing
with signals from waveform digitisers.

# Installation
First, clone the git repository to your local machine.
```
git clone https://bitbucket.org/ttriumfdaq/iris-daqtools.git
```
If you're having trouble with the cloning, you can also download the code as a
zip-file.

Then go to the project folder, create a build folder, configure and build the
project
```
  mkdir build
  cd build
  cmake ..
  make
```
The project builds two libraries: A static library (libdaqtools_static.a) and a
shared library (libdaqtools.so). They provide the same functions, and which of
them you use is entirely up to you. The shared library is particularly useful
is you want to use the tools in a ROOT macro, while linking against the static
library can make your life easier when compiling code, avoiding dynamic linking
for instance.
