#ifndef DISCRIMINATOR_H
#define DISCRIMINATOR_H
#include <vector>

/**
 * Abstract base class for discrimitor implementations.
 */
class Discriminator {
  protected:
    double fDeadTime;              /**< Minimum distance between triggers.*/ 
    std::vector<double> fTriggers; /**< Result of the latest Analyse() call.*/
  
  public:
    Discriminator(double deadTime = 0);
    virtual ~Discriminator() = default;
    
    /**
     * The 'workhorse' method that applies the dicrimination algorithm. The
     * trigger positions can be obtained later with the GetTriggers() method.
     * @param signal Pointer to the beginning of the array containing the signal
     *               to be analysed.
     * @param length Length of the signal to be analysed.
     * @return Number of 'triggers' found in the signal.
     */
    virtual int Analyse(double *signal, int length = 512) = 0;
    
    /**
     * Get the positions of the triggers found in the lates call of Analyse().
     * @return Vector containing the trigger positions.
     */
    std::vector<double> & GetTriggers();
    
    /**
     * Set a 'dead time' for the algorithm. This puts a minimum on the distance
     * between two triggers, allowing precise control of the dead time.
     */
    void SetDeadTime(double deadTime);
    
    double GetDeadTime();
};
#endif
