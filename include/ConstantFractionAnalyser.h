#ifndef CONSTANT_FRACTION_ANALYSER_H
#define CONSTANT_FRACTION_ANALYSER_H
#include "Analyser.h"

/**
 * Class that implements a constant fraction algorithm to obtain time
 * information from a shaped signal. In constrast with the leading edge method,
 * the timing should in principle be independent of the signal amplitude.
 */
class ConstantFractionAnalyser : Analyser {
  private:
    double fAttenuation; /**< Attenuation factor.*/
    int    fDelay;       /**< Delay.*/
    
  public:
    /**
     * Constructor. See documentation of SetAttenuation(), SetDelay() and 
     * SetPolarity() for the description of the arguments.
     */
    ConstantFractionAnalyser
      (double attenuation, int delay, bool negative=false);
    ~ConstantFractionAnalyser() = default;
    
    /**
     * Apply the CFD algorithm. The CFD algorithm works by producing a signal
     * which is composed of the attenuated original signal subtracted by the
     * original signal delayed. The algorithm then looks for zero-crossings.
     * The timing of the zero-crossing should in principle be independent of the
     * amplitude of the input pulse.
     * @param signal Pointer to the begining of the trace to be analysed.
     * @param length Length of the signal to be analysed.
     * @return Time of the zero-crossing.
     */
    double Analyse(double *signal, int length = 512);
    
    /**
     * Attenuation used in the CFD algorithm. The original signal is multiplied
     * by the attenuation factor given here before the delayed signal is
     * subtracted.
     * @param attenuation Attenuation factor used in the CFD. Should be between
     *                    zero and one.
     */
    void SetAttenuation(double attenuation);
    
    double GetAttenuation();

    /**
     * Set delay used by the CFD algorithm. The original signal is delayed by
     * this amount before being subtracted from the attenuated signal.
     * @param delay Delay. The delay can not be longer than the pulse width.
     */    
    void SetDelay(int delay);
    
    int GetDelay();
};
#endif
