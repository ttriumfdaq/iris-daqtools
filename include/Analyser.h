#ifndef ANALYSER_H
#define ANALYSER_H

/**
 * Abstract base class for trace analysers.
 */
class Analyser {
  protected:
    double fResult; /**< Result of the latest call of Analyse().*/
    bool fPolarity; /**< Polarity of the expected signals.*/
    
  public:
  
    /**
     * Constructor.
     * @param negative Expected polarity of the signals to be analysed. Default
     *                 value is false, which means positive polarity signals
     *                 are expected.
     */
    Analyser(bool negative = false);
    virtual ~Analyser() = default;
    
    /**
     * All child classes must implement the Analyse() method.
     * @param signal Pointer to the beginning of the array that will be analysed.
     * @param length Length of the signal (in samples) that will be analysed.
     * @return Result of the applied analysis. Can also be obtained later by
     *         calling GetResult().
     */
    virtual double Analyse(double *signal, int length = 512) = 0;
    
    /**
     * Set polarity of the expected input signal.
     * @param negative Expected polarity of the signals to be analysed. Default
     *                 value is false, which means positive polarity signals
     *                 are expected.
     */
    void SetPolarity(bool negative);
    
    bool GetPolarity();
    
    /**
     * @return Result of latest call of the Analyse() method.
     */
    double GetResult();
};
#endif
