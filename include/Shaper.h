#ifndef SHAPER_H
#define SHAPER_H
#include <cstdint>
#include <vector>

/**
 * Astract base class for shaping algorithms of digitised waveforms. Waveform
 * shaping can be useful to improve energy/time resolution or enabling handling
 * of multiple hits recorded in a single waveform trace.
 */
class Shaper {
  protected:    
    std::vector<double> fShaped; /**< The result of the latest Shape() action.*/
    
  public:
    Shaper() = default;
    virtual ~Shaper() = default;
    
    /**
     * This function must be implemented by classes inheriting from Shaper. This
     * is the 'workhorse', i.e. the function that actually implements the
     * shaping algorithm.
     * @param signal Pointer to the beginning of the array containing the raw
     *               waveform data.
     * @param length Length of the raw waveform in samples.
     * @return Pointer to the array containing the result of the shaping
     *         algorithm.
     */
    virtual double * Shape(double *signal, int length = 512) = 0;

    /**
     * Type conversion. Could possibly be replaced by a template-based solution.
     */
    virtual double * Shape(uint32_t *signal, int length = 512);

    /**
     * Type conversion. Could possibly be replaced by a template-based solution.
     */    
    virtual double * Shape(int *signal, int length = 512);
    
    /**
     * Obtain a vector holding the result of the latest application of Shape().
     * @return Reference to the fShaped member.
     */
    std::vector<double> & GetShaped();
};
#endif
