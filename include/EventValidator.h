#ifndef EVENT_VALIDATOR_C
#define EVENT_VALIDATOR_C
#include <vector>
#include <string>
#include <memory>
#include <stdint.h> //uint32_t
#include "ModuleEvent.h"

class EventValidator {
  private:
    std::vector<std::shared_ptr<ModuleEvent>> moduleEvents;
    uint8_t tolerance;  //How much the IDs of a consistent event can differ.
    uint8_t roll_over;  //How many bits the IDs maximally consist of.
    
  public:
    EventValidator(std::string type, int n);
    ~EventValidator() = default;
    
    //Try to push a word into the event Validator.
    int TryPush(int i, const uint32_t &word){ return moduleEvents.at(i)->TryPush(word);};
    
    //Is the event from module i complete?
    bool Complete(int i) { return moduleEvents.at(i)->Complete();};
    
    //Is the event complete?
    bool Complete();
    
    //Compares timestamps for all ADC events.
    bool CheckConsistency();
    
    std::vector<std::shared_ptr<ModuleEvent>> & GetEvents() {return moduleEvents;};
    
    //If event was not consistent, remove the violating part.
    void Purge();

    //Allow event IDs to vary within a tolerance when checking consistency.
    void SetTolerance(uint8_t delta);
    
    //Set the effective width of the event ID (ADC: 30/46(ext)bit, TDC: 27(ext)bit)
    void SetRollOver(uint8_t nBits);

    uint8_t GetTolerance();

    uint8_t GetRollOver();

    bool Empty();

    void Clear();
};
#endif //EVENT_VALIDATOR_H
