#ifndef ADC_EVENT_H
#define ADC_EVENT_H
#include <queue>
#include <stdint.h> //uint32_t
#include <atomic>
#include "ModuleEvent.h"

class AdcEvent : public ModuleEvent {
  private:
    std::atomic<uint32_t> expectedWords;
    
  public:
    AdcEvent() {};
    ~AdcEvent() = default;
    
    //Try to push. fails if event is already complete.
    virtual int TryPush(const uint32_t &word);
};
#endif //ADC_EVENT_H
