#ifndef AMPLITUDE_ANALYSER_H
#define AMPLITUDE_ANALYSER_H
#include "Analyser.h"

/**
 * This class will analyse the height of a given input signal, useful for
 * extracting energies from shaped pulses, for instance.
 */
class AmplitudeAnalyser : public Analyser {
  public:
    /**
     * Constructor.
     * @param negative See documentation for SetPolarity().
     */
    AmplitudeAnalyser(bool negative = false);
    ~AmplitudeAnalyser() = default;

    /**
     * Finds the maximum amplitude of the given signal.
     * @param signal Pointer to beginning of the array to be analysed.
     * @param length Length of the array to be analysed.
     * @return Result of the analysis. Can also be obtained with GetResult().
     */    
    double Analyse(double *signal, int length = 512);
};
#endif
