#ifndef SAFE_QUEUE_H
#define SAFE_QUEUE_H
#include <queue>
#include <pthread.h>

/**
 * A thread safe version of std::queue. Operations modifying the underlying
 * container are guarded by a mutex lock. Probably very inefficient.
 */
template <class T>
class SafeQueue {
  private:
    std::queue<T> buffer;
    pthread_mutex_t mutex;
    
  public:
    SafeQueue() {mutex = PTHREAD_MUTEX_INITIALIZER;}
    ~SafeQueue() = default;
    
    //Add element to the back of the queue (modifies container).
    void push(const T &val){
      //For some reason the implementation has to be in the header file.
      pthread_mutex_lock(&mutex);
      buffer.push(val);
      pthread_mutex_unlock(&mutex);
    };

    //Pop element from the front of the queue.
    void pop(){ //Modifies container.
      //For some reason the implementation has to be in the header file.
      pthread_mutex_lock(&mutex);
      buffer.pop();
      pthread_mutex_unlock(&mutex);
    };

    //Oldest element in the queue.
    T front() {
      pthread_mutex_lock(&mutex);
      T t = buffer.front();
      pthread_mutex_unlock(&mutex);
      return t;
    };
    
    //Newest element in the queue.
    T back() {
      pthread_mutex_lock(&mutex);
      T t = buffer.back();
      pthread_mutex_unlock(&mutex);
      return t;
    };
    
    bool empty() {
      pthread_mutex_lock(&mutex);
      bool ans = buffer.empty();
      pthread_mutex_unlock(&mutex);     
      return ans;
    };
    
    size_t size() {
      pthread_mutex_lock(&mutex);
      size_t ans = buffer.size();
      pthread_mutex_unlock(&mutex);     
      return ans;
    };

    //Clear the queue.
    void clear(){ //Modifies container.
      pthread_mutex_lock(&mutex);
      std::queue<T> empty;
      std::swap(buffer, empty);
      pthread_mutex_unlock(&mutex);
    };
};
#endif //SAFE_QUEUE_H
