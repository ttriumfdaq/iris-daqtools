#ifndef LEADING_EDGE_DISCRIMINATOR_H
#define LEADING_EDGE_DISCRIMINATOR_H
#include "Discriminator.h"

/**
 * This class implements a leading edge type discriminator. It simply looks for
 * instances where the input signals rises (drops) above (below) the provided
 * trigger threshold.
 */
class LeadingEdgeDiscriminator : public Discriminator {
  protected:
    double fThreshold; /**< Trigger threshold.*/
    bool   fPolarity;  /**< Polarity of the input signal.*/
    
  public:
    /**
     * Constructor.
     * @param threshold See documentation of SetThreshold().
     * @param negative See documentation of SetPolarity().
     */
    LeadingEdgeDiscriminator(double threshold = 0, bool negative = false);
    ~LeadingEdgeDiscriminator() = default;
    
    virtual int Analyse(double *signal, int length = 512);
    
    /**
     * Provide trigger threshold to be used by the algorithm. When the signal
     * rises above the theshold (or drops below, depending on the polarity), it
     * is registered as a trigger.
     * @param threshold Threshold.
     */
    void SetThreshold(double threshold);
    
    /**
     * Set the polarity of the expected input pulses. If the pulses are positive
     * the algorithm should look for times when the signal rises above the
     * threshold.
     * @param negative Polarity. false for positive polarity signals.
     */
    void SetPolarity(bool negative);
    
    double GetThreshold();
    
    bool GetPolarity();
};  
#endif
