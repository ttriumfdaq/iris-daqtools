#ifndef TRAPEZOID_SHAPER_H
#define TRAPEZOID_SHAPER_H
#include "Shaper.h"

/**
 * Implementation of the Shaper class. The TrapezoidShaper applies a trapezoidal
 * shaping algorithm to the input waveform. It expects an input which is a
 * right-sided exponentially decaying signal with a specified decay time (in
 * units of data samples) and produces a trapezoidal pulse with a specified
 * rise time and top width (again, these parameters must be given in units of
 * data samples). The baseline of the shaped signal should be around zero and
 * the height of the trapezoidal pulse should correspond to the step height
 * of the input exponential signal. See also [1].
 *
 * [1] Jordanov, V.T. and Knoll, G.F., Nucl. Inst. and Meth. A 345, 337 (1994)
 */
class TrapezoidShaper : public Shaper {
  private:
    int fDecayTime; /**< Expected decay time of the raw signals.*/
    int fRiseTime;  /**< Requested rise time (~shaping time) of the trapezoid.*/
    int fTopWidth;  /**< Requested top width of the trapezoid.*/

    double d(double *signal, int k, int l, int j);
    
  public:
    /**
     * Constructor.
     * @param decayTime See documentation of SetDecayTime().
     * @param riseTime See documentation of SetRiseTime().
     * @param topWidth See documentation of SetTopWidth().
     */
    TrapezoidShaper(int decayTime, int riseTime, int topWidth);
    virtual ~TrapezoidShaper() = default;
 
    double * Shape(double *signal, int length = 512);
    using Shaper::Shape;
    
    /**
     * Supply the expected decay time of the raw input signal. This is the time
     * (in number of data samples) it takes for the input signal to decay to 1/e
     * of its original amplitude. Improper values can result in a bad pole-zero.
     * @param time Expected decay time.
     */
    void SetDecayTime(int time);
    
    /**
     * Supply the wanted rise time of the trapezoidal output pulse. This is in
     * some sense similar to the 'shaping time' traditionally set on the
     * physical shaping amplifiers used in nuclear physics experiments. A longer
     * shaping time can give better resolution, but will also limit the ability
     * to handle high count rates, i.e. several pulses arriving shortly after
     * one another.
     * @param time Wanted rise time of the trapezoid.
     */
    void SetRiseTime(int time);
    
    /**
     * Supply the wanted top width of the trapezoidal output pulse.
     * @param width Wanted top width.
     */
    void SetTopWidth(int width);
    
    int GetDecayTime();
    int GetRiseTime();
    int GetTopWidh();
};
#endif
